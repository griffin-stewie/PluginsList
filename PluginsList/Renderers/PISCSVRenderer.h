//
//  PISCSVRenderer.h
//  PluginsList
//
//  Created by griffin-stewie on 2019/09/27.
//  Copyright © 2019 cyan-stivy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PISRenderer.h"

extern NSString * const PISExportFormatCSV;

NS_ASSUME_NONNULL_BEGIN

@interface PISCSVRenderer : PISRenderer

@end

NS_ASSUME_NONNULL_END
