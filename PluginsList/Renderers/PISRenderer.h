//
//  PISRenderer.h
//  PluginsList
//
//  Created by griffin-stewie on 2019/09/28.
//  Copyright © 2019 cyan-stivy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PISRendererProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface PISRenderer : NSObject<PISRendererProtocol>

@end

NS_ASSUME_NONNULL_END
