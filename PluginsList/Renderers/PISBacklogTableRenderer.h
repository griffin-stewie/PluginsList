//
//  PISBacklogTableRenderer.h
//  PluginsList
//
//  Created by griffin-stewie on 2019/09/27.
//  Copyright © 2019 cyan-stivy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PISRenderer.h"

extern NSString * const PISExportFormatBacklog;

NS_ASSUME_NONNULL_BEGIN

@interface PISBacklogTableRenderer : PISRenderer

@end

NS_ASSUME_NONNULL_END
