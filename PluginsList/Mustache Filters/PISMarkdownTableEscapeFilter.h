//
//  PISMarkdownTableEscapeFilter.h
//  PluginsList
//
//  Created by griffin-stewie on 2019/09/27.
//  Copyright © 2019 cyan-stivy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GRMustache/GRMustache.h>

NS_ASSUME_NONNULL_BEGIN

@interface PISMarkdownTableEscapeFilter : NSObject<GRMustacheFilter>

@end

NS_ASSUME_NONNULL_END
