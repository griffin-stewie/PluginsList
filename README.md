# PluginsList

This [Sketch](https://www.sketchapp.com/) plugin gives you exporting plugins informations as text.

## Install

- Downloads latest plugin from [here](https://github.com/griffin-stewie/PluginsList/releases/latest).
- Unzip it, and doble click `PluginsList.sketchplugin`

## How it works

1. Run `Plugins > Plugins List > Export as...`
1. Select text format from PopUp Button.
1. Choose `Copy to pasteboard` or `Save to file...`

## Credits

- [griffin-stewie](https://griffin-stewie.github.io/): Developer
- [Poem](https://dribbble.com/poem_f): Icon Designer
